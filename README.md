# Statistical Learning I
========================

## Mini project 1
-----------------

#### Problem 1 (KNN)
Consider the training and test data posted on Canvas in the files `training_data_knn_mp1.csv` and  `test_data_knn_mp1.csv`, respectively, for a classification problem with two classes.  

a. Fit KNN with $`K = 1, 2, 3,\ldots, 35,50,100,150`$. 
b. Plot training and test error rates against $`1/K`$. Explain what you observe. Is it consistent with what you expect from the class? 

c. What is the optimal value of $`K`$ (if there are several such $`K`$'s pick the smallest one)? What are the training and test error rates associated with the optimal $`K`$?
d. Make a plot of the training data that also shows the decision boundary for the optimal $`K`$ and the _Bayes_ decision boundary from problem 6 on HW 1. Comment on what you observe. Does the decision boundary seem sensible when compared to the Bayes decision boundary? 

#### Problem 2 (Modified KNN)
Let $`x`$ be a point in $`\mathbb{R}^d`$ (in our case $`d`$=2). Using the usual KNN, we would assign a class label to $`x`$ based on the majority vote. On this problem, we'll introduce an alternative decision rule which takes into account the distances between $`x`$ and its $`K`$ nearest neighbors. 

Let $`x_1,x_2,\ldots,x_K`$ be the $`K`$ nearest neighbors of $x$ that belong to the training set (if there are ties for the $`K`$th nearest
neighbor then all those points are included) and let $`y_1,y_2,\ldots,y_K`$ be their class labels (in our case 0 or 1). 

Let $`N_0=|\{i:y_i=0\}|$ and $N_1=|\{i:y_i=1\}|$. Note that $N_0+N_1=K`$.

__Decision rule__: 
1. If $`N_0=0`$ then assign class 1 to $`x`$.
2. If $`N_1=0`$ then assign class 0 to $`x`$.
3. If $`N_0>0`$ and $`N_1>0`$ then assign class 1 to $`x`$ if

$`\frac{1}{N_1}\sum_{i:y_i=1} \|x-x_i\|_2 < \frac{1}{N_0}\sum_{i:y_i=0} \|x-x_i\|_2 `$

and class 0 if the above inequality sign is flipped. If the two sums are equal, break the ties at random. 

Write a function `modified_knn()` which receives the same set of arguments as the usual `knn()` function (`train`, `test`, `cl` and `k`) from the `class` package and outputs a sequence of predicted classes using the new decision rule. Repeat parts (a)-(c) of problem 1 for the modified KNN and compare the test error rates associated with the optimal $`K`$'s of the two approaches. 

#### Solution[[ R code](Notebooks/Mini-project_-1Jishan.Rmd)]


## Mini project 2 
-----------------
Consider the crime data stored in `crime.csv`. We would like to understand how murder rate is related to the other variables in the dataset. Note that `state` is the "subject" here; it's not a predictor, and `region` is a qualitative variable.

1. Build a multiple linear regression model to predict murder rate based on the other variables. The final best model should have all the important variables and it should not have any unimportant variables (use the `anova()` function to compare nested models and to decide which set of predictors can be dropped). Once all the important variables are identified, be sure to explore the significance of pairwise interactions between them as well (using the `anova()` function again). Finally, perform model diagnostics to check the standard model assumptions (normality, constant variance and independence of errors) using the visualization tools (normal QQ plot, residual plot and time series plot) and perform any transformations needed to obtain a model for which the assumptions reasonably hold (e.g. if the variance of errors increases when plotted against fitted values, try transforming $`Y`$ (murder rate) using `log()` or `sqrt()` functions).

2. Use your final model to predict murder rate of a state whose predictor values are set at the average in the data for a quantitative predictor and the most frequent category for a qualitative predictor.

#### Solution[[ R code](Notebooks/Mini_project_2.Rmd)]


## Mini project 3 
-----------------

#### Problem 1 (LDA and QDA)

Consider the business school admission data available in `admission.csv`. The admission officer of a business school has used an "index" of undergraduate grade point average ($`X_1`$=`GPA`) and graduate management aptitude test ($`X_2`$=`GMAT`) scores to help decide which applicants should be admitted to the school's graduate programs. This index is used to categorize each applicant into one of three groups - `admit` (group 1), `do not admit` (group 2), and `borderline` (group 3). Take the _last five_ observations in each category as test data and the remaining observations as training data.

a. Produce a scatter plot of `GMAT` against `GPA` by coloring the points according to the applicants' groups. Also, produce boxplots of `GMAT` and `GPA` for each group. Comment on how helpful these predictors may be in predicting the response.  

b. Fit `LDA` to the training data. Compute the `confusion matrix` and the `error rate` for both training and test data. What do you observe?

c. Repeat part (b) for `QDA`.

d. Compare the results in parts (b) and (c). Which of the two classifiers would you recommend? Why do you think `QDA` performs better?

#### Problem 2 (Logistic Regression, LDA and QDA)

Annual financial data are collected for bankrupt firms approximately two years prior to their bankruptcy and financially sound firms at about the same time. The data on four variables, $`X1 = CF/TD`$=(cash flow)/(total debt), $`X2 = NI/TA`$=(net income)/(total assets), $`X3 = CA/CL`$=(current assets)/(current liabilities), and $`X4 = CA/NS`$=(current assets)/(net sales) are given in `bankruptcy.csv`. This is a binary classification problem. Take bankrupt firm as "+" response (indicated as 0 in the data) and non-bankrupt firm as "-" response (indicated as 1 in the data). Use all the data as training data.

a. Fit `logistic regression` to the training data using all the predictors. Look at the summary of this fit and decide which two predictors might be jointly dropped. Verify your decision by performing the anova chi-square test to obtain a final reduced model. 

b. Compute the error rate, sensitivity, specificity and AUC for your final model.

c. Find the equation of the decision boundary.

d. Produce a scatter plot of the predictors in your final model (there should be two such predictors) and superimpose the decision boundary. Color the points according to the value of the response. Comment on what you observe.

e. Fit `LDA` to the training data using all the predictors. Compute the error rate, sensitivity, specificity and AUC.

f. Repeat part (e) for `QDA`.

g. Put all the error rate, sensitivity, specificity and AUC results into a single table and print to screen. Which classifier would you recommend? Justify your conclusions.

#### Solution[[ R code](Notebooks/Mini_project_3.Rmd)]


## Mini project 4
-----------------

Consider the Prostate Cancer data available in `ElemStatLearn` package as `prostate` (note: since `ElemStatLearn` is currently removed from the CRAN repository, to install formerly available versions of the package visit `https://cran.r-project.org/src/contrib/Archive/ElemStatLearn/`, download the most recent `.tar.gz` archive file and install it by typing in the console `install.packages("a path to the archive file", repos = NULL, type = "source")`). Look at the data description using `?prostate`. You are also encouraged to read about these data on pages 3-4 and 49-51 and in Section 3.4 of the book The Elements of Statistical Learning, 2nd edition, available from `https://web.stanford.edu/~hastie/ElemStatLearn/`. We will take `lpsa` as the response and the following 8 variables are predictors: `lcavol`, `lweight`, `age`, `lbph`, `svi`, `lcp`, `gleason`, and
`pgg45`. Note that the book divides the dataset into training and test sets. However, we will take all the observations as training data. Further, for any fitting method (see below) that has a complexity
parameter (e.g. `lambda` in ridge regression), choose the best value for the parameter using estimated test $`RMSE`$ (i.e. $`\sqrt{MSE}`$) based on 10-fold cross-validation repeated 30 times (`createMultiFolds()` function in `caret` package can help you construct these 10x30=300 folds. Use the same folds for all the models mentioned below).

(a) Fit a linear model using all the predictors with the usual least squares.
(b) Fit a linear model using best-subset selection. Use $`C_p`$ to compare models of different sizes.
(c) Fit a linear model using ridge regression. Use $`\lambda=0,0.001,0.01,0.1,1,10,100,1000`$.
(d) Fit a linear model using lasso. Use $`\lambda=0,0.001,0.01,0.1,1,10,100,1000`$.
(e) Fit a linear model using PCR.
(f) Fit a linear model using PLS.
(g) Make a table containing estimated test $`RMSE`$'s and the best parameter values for all the methods used. Compare the results. Which method would you recommend? 

#### Solution[[ R code](Notebooks/Mini_project_4_SolnJishan.Rmd)]
